using System.Collections.Generic;

namespace DataAccess
{
	public interface IWordsRepository
	{
		void Add(Word word);
		IEnumerable<Word> FindAll();
	}
}