﻿using System;

namespace DataAccess
{
	public class Shot
	{
		public int Id { get; set; }
		public Word Word { get; set; }
		public bool Result { get; set; }
		public DateTime Date { get; set; }
	}
}
