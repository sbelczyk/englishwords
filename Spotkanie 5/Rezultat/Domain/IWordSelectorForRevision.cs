﻿using System.Collections.Generic;
using DataAccess;

namespace Domain
{
	public interface IWordSelectorForRevision
	{
		IEnumerable<Word> GetWordsForRevision();
	}
}
