﻿using System;
using System.Collections.Generic;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Tests
{
	[TestClass]
	public class Top10WordsSelectorTests
	{
		private Mock<IWordsRepository> wordsrepo = new Mock<IWordsRepository>();

		[TestMethod]
		public void GetWordsForRevision_returns_null_if_no_words_in_repository()
		{
			//given
			var wordsSelecgtor = new Top10WordsSelector(wordsrepo.Object);
			wordsrepo.Setup(x => x.FindAll()).Returns(new List<Word>
				                                          {
					                                          new Word(),new Word(),new Word(),new Word(),new Word(),new Word(),new Word(),new Word()
				                                          });
			//when
			var words = wordsSelecgtor.GetWordsForRevision();

			//then
			Assert.AreEqual(8, words.Count());


		}

		[TestMethod]
		public void GetWordsForRevision_throws_exception_if_there_is_less_then_10_words_in_repo()
		{
			//given
			var wordsSelecgtor = new Top10WordsSelector(wordsrepo.Object);
			wordsrepo.Setup(x => x.FindAll()).Returns(new List<Word>
				                                          {
					                                          new Word()
				                                          });
			//when
			try
			{
				var words = wordsSelecgtor.GetWordsForRevision();


				Assert.Fail("fasdf");
			}
			catch (Exception ex)
			{
				Assert.IsTrue(true);
			}



		}

		[TestMethod]
		public void GetWordsForRevision_calls_findall_in_repo()
		{
			//given
			var wordsSelecgtor = new Top10WordsSelector(wordsrepo.Object);
			wordsrepo.Setup(x => x.FindAll()).Returns(new List<Word>
				                                          {
					                                          new Word(),new Word(),new Word(),new Word(),new Word(),new Word(),new Word(),new Word()
				                                          });
			//when
			var words = wordsSelecgtor.GetWordsForRevision();

			//then
			wordsrepo.Verify(x=>x.FindAll());


		}
	}
}
