﻿using System.Data.Entity;

namespace DataAccess
{
	public class WordsDB : DbContext
	{
		public WordsDB()
			: base("connectionString")
		{
			
		}

		public DbSet<Word> Words { get; set; }
		public DbSet<Shot> Shots { get; set; }
	}
}
