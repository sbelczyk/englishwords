namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Words",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EnglishWord = c.String(),
                        Translation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Shots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Result = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Word_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Words", t => t.Word_Id)
                .Index(t => t.Word_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Shots", new[] { "Word_Id" });
            DropForeignKey("dbo.Shots", "Word_Id", "dbo.Words");
            DropTable("dbo.Shots");
            DropTable("dbo.Words");
        }
    }
}
