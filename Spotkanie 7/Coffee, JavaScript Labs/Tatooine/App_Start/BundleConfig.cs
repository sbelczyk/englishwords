﻿using System.Web;
using System.Web.Optimization;

namespace Tatooine
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
						"~/Scripts/jquery-ui-{version}.js"));


			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

			bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
						"~/Content/themes/base/jquery.ui.core.css",
						"~/Content/themes/base/jquery.ui.resizable.css",
						"~/Content/themes/base/jquery.ui.selectable.css",
						"~/Content/themes/base/jquery.ui.accordion.css",
						"~/Content/themes/base/jquery.ui.autocomplete.css",
						"~/Content/themes/base/jquery.ui.button.css",
						"~/Content/themes/base/jquery.ui.dialog.css",
						"~/Content/themes/base/jquery.ui.slider.css",
						"~/Content/themes/base/jquery.ui.tabs.css",
						"~/Content/themes/base/jquery.ui.datepicker.css",
						"~/Content/themes/base/jquery.ui.progressbar.css",
						"~/Content/themes/base/jquery.ui.theme.css"));

			bundles.Add(new ScriptBundle("~/Scripts/colorpicker").Include(
						"~/colorpicker/js/colorpicker.js",
						"~/colorpicker/js/eye.js",
						"~/colorpicker/js/utils.js",
						"~/colorpicker/js/layout.js"				
				));
			var robbo = new[]{
				
				"~/Scripts/Lab3/Module.js"
				,"~/Scripts/Lab3/TimeDelayedMethodCall.js"
				,"~/Scripts/Lab3/ColorManager.js"
				,"~/Scripts/Lab3/EventAggregator.js"
				,"~/Scripts/Lab3/MapLoader.js"
				,"~/Scripts/Lab3/Game.js"
				,"~/Scripts/Lab3/Mixins/AssetLoader.js"

				,"~/Scripts/Lab3/DrawingContext.js"
				,"~/Scripts/Lab3/EnvironmentContext.js"

				,"~/Scripts/Lab3/Watchers/AmmoWatcher.js"
				,"~/Scripts/Lab3/Watchers/KeyboardWatcher.js"
				,"~/Scripts/Lab3/Watchers/BoltWatcher.js"
				,"~/Scripts/Lab3/Watchers/KeysWatcher.js"
				,"~/Scripts/Lab3/LevelManager.js"


				,"~/Scripts/Lab3/Mixins/Moveable.js"
				,"~/Scripts/Lab3/Mixins/Animatable.js"
				,"~/Scripts/Lab3/Mixins/Multistate.js"
				,"~/Scripts/Lab3/Mixins/Blowable.js"
				,"~/Scripts/Lab3/Mixins/Bombblowable.js"
				,"~/Scripts/Lab3/Mixins/Collectable.js"

				,"~/Scripts/Lab3/Objects/Object.js"
				,"~/Scripts/Lab3/Objects/Rubble.js"
				,"~/Scripts/Lab3/Objects/Door.js"
				,"~/Scripts/Lab3/Objects/Bomb.js"
				,"~/Scripts/Lab3/Objects/Bolt.js"
				,"~/Scripts/Lab3/Objects/Container.js"
				,"~/Scripts/Lab3/Objects/Robbo.js"
				,"~/Scripts/Lab3/Objects/Wall.js"
				,"~/Scripts/Lab3/Objects/Ammo.js"
				,"~/Scripts/Lab3/Objects/Ship.js"
				,"~/Scripts/Lab3/Objects/Key.js"
				,"~/Scripts/Lab3/Objects/LaserBeam.js"
				,"~/Scripts/Lab3/Objects/Smoke.js"
				,"~/Scripts/Lab3/Objects/Laser.js"
			};

			var robboBundle = new ScriptBundle("~/bundles/lab3").Include(robbo);
			robboBundle.Include("~/Scripts/Lab3/GameLoader.js");

			var robboTestBundle = new ScriptBundle("~/bundles/lab3-tests").Include(robbo);
			robboTestBundle.Include("~/Scripts/Lab3/Tests/TestHelpers.js");
			robboTestBundle.Include("~/Scripts/Lab3/Tests/MoveableMixinTests.js");
			robboTestBundle.Include("~/Scripts/Lab3/Tests/MultistateMixinTests.js");
			robboTestBundle.Include("~/Scripts/Lab3/Tests/CollectableMixinTest.js");
			robboTestBundle.Include("~/Scripts/Lab3/Tests/BombblowableMixinTest.js");

			bundles.Add(robboBundle);
			bundles.Add(robboTestBundle);
		}
	}
}