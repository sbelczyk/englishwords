﻿using System.Web.Mvc;

namespace Tatooine.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {           
            return View();
        }


        public ActionResult Exercise(int lab, int ex)
        {
            return View("Lab"+lab.ToString()+"/Ex"+ex.ToString());
        }


	    public ActionResult ContainerTests()
	    {
		    return  View("Lab3/Ex1_tests");

		}


    }
}
