﻿$(function () {
    var text = "Imię says: ";
    $(".set-name-btn").click(function () {
        var imie = prompt("Podaj imię:");
        var output = text + imie;
        if (imie.toLocaleLowerCase() == "simon") {
            output = "Simon says: architecture!";
        }
        $(".set-name-btn").text(output);
    });
})