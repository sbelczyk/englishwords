﻿var getCemeteries, service_url;

service_url = 'http://niagaraodi.cloudapp.net:8080/v1/Niagara%20Open%20Data/Cemeteries/?format=json&$select=CEM_NAME,Long,Lat,OWNERSHIP,MUNICIPALITY';

getCemeteries = function (callback) {
	return $.getJSON(service_url + '&callback=?', {}, function (response) {
		return callback(response.d);
	});
};

$(function() {

    getCemeteries(function (data) {
        var cementries = new Array();
        var cementriesWithoutOwner = 0;
        var cementriesInLincoln = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i].MUNICIPALITY.toLocaleLowerCase().contains("lincoln")) {
                cementriesInLincoln++;
                cementries.push(data[i]);
            }
            
            if (data[i].OWNERSHIP.length == 0) {
                cementriesWithoutOwner++;
            }
        }
        $('.console').text("Cmentarze bez właściciela: " + cementriesWithoutOwner +"\nCmentarze w Lincoln: " + cementriesInLincoln);
        showTop5Cementries(cementries);
    });
});

function showTop5Cementries(data) {
    var template = $(".template");
    $(".template").remove();
    for (var i = 0; i < 5; i++) {
        var item = template.clone();
        item.find("[data-for='CEM_NAME']").append(data[i].CEM_NAME);
        item.find("[data-for='MUNICIPALITY']").append(data[i].MUNICIPALITY);
        item.find("[data-for='OWNERSHIP']").append(data[i].OWNERSHIP);
        item.find("[data-for='Lat']").append(data[i].Lat);
        item.find("[data-for='Long']").append(data[i].Long);
        $(".cemeteries").append(item);
    }

}

Array.prototype.where = function (predicat) {
	var item, _i, _len, _results;
	_results = [];
	for (_i = 0, _len = this.length; _i < _len; _i++) {
		item = this[_i];
		if (predicat(item)) {
			_results.push(item);
		}
	}
	return _results;
};

Array.prototype.top = function (n) {
	return this.slice(0, n);
};

