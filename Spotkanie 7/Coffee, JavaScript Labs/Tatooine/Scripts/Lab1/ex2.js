﻿function sortElements(a, b) {
    return $(a).data('index') > $(b).data('index') ? 1 : -1;
};


$(function () {
    $(".sort-btn").click(function () {
        $('.st-box').sort(sortElements).appendTo('.movies');
        $('.st-box').each(function (index) {
            $(this).prepend('<h3>' + $(this).attr('data-index') + "</h3>");
        });
    });
});
