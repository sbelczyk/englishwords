﻿Array.prototype.where = (predicat) ->
	item for item in this when predicat(item)		

Array.prototype.single = (predicat) ->
	items = []
	items.push item for item in this when predicat(item)

	return items[0] unless items.length isnt 1

	return null if items.length is 0

	throw "Many items for given predicat"

Array.prototype.top = (n)-> this.slice(0,n)

Array.prototype.any = (predicat) ->
	items = []
	items.push item for item in this when predicat(item)

	return items.length>0


	
	
