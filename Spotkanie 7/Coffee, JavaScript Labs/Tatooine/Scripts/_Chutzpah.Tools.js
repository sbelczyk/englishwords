
Array.prototype.where = function(predicat) {
  var item, _i, _len, _results;
  _results = [];
  for (_i = 0, _len = this.length; _i < _len; _i++) {
    item = this[_i];
    if (predicat(item)) {
      _results.push(item);
    }
  }
  return _results;
};

Array.prototype.single = function(predicat) {
  var item, items, _i, _len;
  items = [];
  for (_i = 0, _len = this.length; _i < _len; _i++) {
    item = this[_i];
    if (predicat(item)) {
      items.push(item);
    }
  }
  if (items.length === 1) {
    return items[0];
  }
  if (items.length === 0) {
    return null;
  }
  throw "Many items for given predicat";
};

Array.prototype.top = function(n) {
  return this.slice(0, n);
};

Array.prototype.any = function(predicat) {
  var item, items, _i, _len;
  items = [];
  for (_i = 0, _len = this.length; _i < _len; _i++) {
    item = this[_i];
    if (predicat(item)) {
      items.push(item);
    }
  }
  return items.length > 0;
};
