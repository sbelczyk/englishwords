﻿

window.app = window.app ? {}
app = window.app

template = """
<div class='users'>
    <div class='user template item'>
        <h3 class='index'>1</h3>
        <div class='label'>User name: </div>
        <div class='data'>
            <input type='text' class='userName' data-for='userName' />
        </div>
        <div class='label'>Email: </div>
        <div class='data'>
            <input type='text' class='email' data-for='email' />
        </div>
    </div>
</div>
"""



module( "FormList", {
						setup: () ->
							$('.test-playground').html(template)
						teardown: () ->
							$('.test-playground').html()
});

test "when control is created there is only one element", () ->
	#given/when
	new app.FormList $('.users'), 5
	#then
	equal($('.user').size(),1,'thre is only one form item')

test "item is added when user click 'add' button", () ->
	#given/when
	new app.FormList $('.users'), 5
	$($('.addBtn')[0]).click()
	#then
	equal($('.user').size(),2,'thre are two items after user clicked "add"')