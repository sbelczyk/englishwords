var a;

(function () {

  test('quickSort doesn\'t change array if array is sorted', function() {
    var soretedArray;
    soretedArray = [1, 2, 3, 4, 5, 6, 7, 8];
    Array.quicksort(soretedArray);
    return deepEqual(soretedArray, [1, 2, 3, 4, 5, 6, 7, 8]);
  });

  test("sorting empty array doesn't create elements", function () {
      var soretedArray;
      soretedArray = [];
      Array.quicksort(soretedArray);
      return deepEqual(soretedArray, []);
  });

  test("sorting array, which contains string, int and char, casts everything to string and sorts like strings", function () {
      var soretedArray;
      soretedArray = ["dasdsa", 3, 3, 8, 3, 'a'];
      Array.quicksort(soretedArray);
      return deepEqual(soretedArray, ["a", "dasdsa", 3, 3, 3, 8]);
  });

  test("sorting array, which contains string, int, object and char, casts everything to string and sorts like strings", function () {
      var soretedArray;
      var object = new Object();
      object.name = "Trolololo";
      object.value = 32423;
      soretedArray = ["dasdsa", 3, 3, 8, 3, object, "e", "[", "{   }"];
      Array.quicksort(soretedArray);
      return deepEqual(soretedArray, ["[",
              {
                  "name": "Trolololo",
                  "value": 32423
              },
              "dasdsa", "e", "{   }", 3, 3, 3, 8]);
  });

}).call(this);
