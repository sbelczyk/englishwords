﻿## <reference path="QuickSort.coffee" />

test 'quickSort doesn\'t change array if array is sorted', () ->
	#given
	soretedArray = [1,2,3,4,5,6,7,8]
	#when
	Array.quicksort(soretedArray)
	#then
	deepEqual(soretedArray,[1,2,3,4,5,6,7,8])

