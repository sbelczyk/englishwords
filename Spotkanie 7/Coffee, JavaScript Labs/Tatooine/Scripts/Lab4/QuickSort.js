(function() {

  Array.quicksort = function(a) {
    var divide, partition, qs, swap;
    swap = function(i, j) {
      var _ref;
      if (i === j) {
        return;
      }
      return _ref = [a[j], a[i]], a[i] = _ref[0], a[j] = _ref[1], _ref;
    };
    divide = function(v, start, end) {
      var first_big, j;
      first_big = start;
      j = start;
      while (j <= end) {
        if (a[j] < v) {
          swap(first_big, j);
          first_big += 1;
        }
        j += 1;
      }
      return first_big;
    };
    partition = function(start, end) {
      var first_big, v;
      v = a[end];
      first_big = divide(v, start, end - 1);
      swap(first_big, end);
      return first_big;
    };
    qs = function(start, end) {
      var m;
      if (start >= end) {
        return;
      }
      m = partition(start, end);
      qs(start, m - 1);
      return qs(m + 1, end);
    };
    return qs(0, a.length - 1);
  };

}).call(this);
