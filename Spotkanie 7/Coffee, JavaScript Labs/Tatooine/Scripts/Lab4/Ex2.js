(function() {
  var app, template, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  template = "<div class='users'>\n    <div class='user template item'>\n        <h3 class='index'>1</h3>\n        <div class='label'>User name: </div>\n        <div class='data'>\n            <input type='text' class='userName' data-for='userName' />\n        </div>\n        <div class='label'>Email: </div>\n        <div class='data'>\n            <input type='text' class='email' data-for='email' />\n        </div>\n    </div>\n</div>";

  module("FormList", {
    setup: function() {
      return $('.test-playground').html(template);
    },
    teardown: function() {
      return $('.test-playground').html();
    }
  });

  test("when control is created there is only one element", function() {
    new app.FormList($('.users'), 5);
    return equal($('.user').size(), 1, 'thre is only one form item');
  });

  test("item is added when user click 'add' button", function() {
    new app.FormList($('.users'), 5);
    $($('.addBtn')[0]).click();
    return equal($('.user').size(), 2, 'thre are two items after user clicked "add"');
  });

}).call(this);
