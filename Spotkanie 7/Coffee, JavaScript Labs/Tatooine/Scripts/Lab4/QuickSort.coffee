﻿Array.quicksort = (a) ->
  swap = (i, j) ->
    return if i == j
    [a[i], a[j]] = [a[j], a[i]]
 
  divide = (v, start, end) ->
    first_big = start
    j = start
    while j <= end
      if a[j] < v
        swap first_big, j
        first_big += 1
      j += 1
    first_big
 
  partition = (start, end) ->
    v = a[end]
    first_big = divide v, start, end-1
    swap first_big, end
    first_big
 
  qs = (start, end) ->
    return if start >= end
    m = partition start, end
    qs start, m-1
    qs m+1, end
 
  qs 0, a.length - 1
 
