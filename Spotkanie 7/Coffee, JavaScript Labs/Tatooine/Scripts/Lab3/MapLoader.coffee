﻿window.app = window.app ? {}
app = window.app

class app.MapLoader
	constructor: (@mapStr,@envCtx)->
		#ctor

	load: ->
		lines = @mapStr.split '\n'
		@envCtx.initMap lines[0].length-2,lines.length
		for line,y in lines
			line = line.substring 2
			for char,x in line
				obj = null
				switch char
					when 'w'
						obj = new app.Wall @envCtx,x,y
					when 'r'
						obj = new app.Robbo @envCtx,x,y
					when 'b'
						obj = if app.Bolt? then  new app.Bolt @envCtx,x,y else null
					when 'c'
						obj = new app.Container @envCtx,x,y
					when 's'
						obj = new app.Ship @envCtx,x,y
					when 'x'
						obj = new app.Bomb @envCtx,x,y
					when 'k'
						obj = if app.Key? then  new app.Key @envCtx,x,y else null 
					when '#'
						obj = new app.Rubble @envCtx,x,y
					when 'd'
						obj = new app.Door @envCtx,x,y
					when 'a'
						obj = new app.Ammo @envCtx,x,y
					when '|'
						obj = new app.LaserBeam @envCtx,x,y,[0,1]
						obj.init()
					when '-'
						obj = new app.LaserBeam @envCtx,x,y,[1,0]
						obj.init()
					when '>'
						obj = new app.Laser @envCtx,x,y,'right'
					when 'v'
						obj = new app.Laser @envCtx,x,y,'down'
					when '<'
						obj = new app.Laser @envCtx,x,y,'left'
					when '^'
						obj = new app.Laser @envCtx,x,y,'up'
				@envCtx.setObjAt(x,y, obj) unless obj==null
		return
		
