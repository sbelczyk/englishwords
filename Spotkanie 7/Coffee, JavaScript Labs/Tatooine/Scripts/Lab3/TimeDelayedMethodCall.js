(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.TimeDelayedMethodCall = (function() {

    function TimeDelayedMethodCall() {
      this.randomCallers = [];
    }

    TimeDelayedMethodCall.prototype.delay = function(time, fun) {
      return setTimeout(fun, time);
    };

    TimeDelayedMethodCall.prototype.registerRandomCall = function(obj, fun) {
      this.randomCallers.push({
        obj: obj,
        fun: fun
      });
      return this.randomTrigger(obj, fun);
    };

    TimeDelayedMethodCall.prototype.unregisterRandomCalls = function(obj) {
      var i, rCall, _i, _len, _ref1;
      _ref1 = this.randomCallers;
      for (i = _i = 0, _len = _ref1.length; _i < _len; i = ++_i) {
        rCall = _ref1[i];
        if ((rCall != null) && (rCall.obj === obj)) {
          this.randomCallers[i] = null;
        }
        this.randomCallers = this.randomCallers.where(function(i) {
          return i != null;
        });
      }
    };

    TimeDelayedMethodCall.prototype.randomTrigger = function(obj, fun) {
      var callback, rc, time,
        _this = this;
      rc = this.randomCallers.single(function(c) {
        return c.obj === obj && c.fun === fun;
      });
      if ((rc != null)) {
        time = this.getRand(1, 15) * 300;
        callback = function() {
          rc = _this.randomCallers.single(function(c) {
            return c.obj === obj && c.fun === fun;
          });
          if ((rc != null)) {
            rc.fun();
            return _this.randomTrigger(rc.obj, rc.fun);
          }
        };
        return setTimeout(callback, time);
      }
    };

    TimeDelayedMethodCall.prototype.getRand = function(x, y) {
      return Math.floor((Math.random() * y) + x);
    };

    return TimeDelayedMethodCall;

  })();

}).call(this);
