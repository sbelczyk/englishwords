﻿window.app = window.app ? {}
app = window.app

mapStr = """
1 wwwwwwwwwwwwwwww
2 wr_b_c_c_______w
3 w_b_c_cc_______w
4 wccc__ccc______w
5 w______cc______w
6 w_____c________w
7 w______________w
8 w_____k________w
9 w______________w
10w______________w
11w______________w
12w______________w
13w______________w
14w______________w
15w______________w
16wwwwwwwwwwwwwwww
"""

app.ColorTranslation = [{from: [162,114,64,255], to:[102,114,64,255]}
						,{from: [28,39,131,255], to:[28,39,129,255]}
						,{from: [16,16,16,255], to:[16,16,16,255]}
						,{from: [152,152,152,255], to:[152,152,152,255]}
						,{from: [0,0,0,0], to:[0,0,0,0]}]

$(window).load ->
		game = new app.Game($('.game-board'),mapStr)
		window.game = game
		new app.ColorManager($('.game-board'),game)

		