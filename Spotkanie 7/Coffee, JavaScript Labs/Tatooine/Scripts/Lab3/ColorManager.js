(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.ColorManager = (function() {

    function ColorManager(canvas, game) {
      var bs, start,
        _this = this;
      this.game = game;
      bs = $('.background-color-selector');
      start = '#' + bs.attr('data-start-val');
      bs.css('background-color', start);
      bs.ColorPicker({
        color: bs.attr('data-start-val'),
        onSubmit: function(hsb, hex, rgb, el) {
          bs.css('background-color', '#' + hex);
          return canvas.css('background-color', '#' + hex);
        }
      });
      $('.color-selector').each(function(i, e) {
        var startVal;
        startVal = $(e).attr('data-start-val');
        $(e).css('background-color', "#" + startVal);
        return $(e).ColorPicker({
          color: "#" + startVal,
          onSubmit: function(hsb, hex, rgb, el) {
            var to;
            $(el).css('background-color', '#' + hex);
            to = app.ColorTranslation[$(el).attr('data-for')].to;
            to[0] = rgb.r;
            to[1] = rgb.g;
            to[2] = rgb.b;
            app.ColorTranslation.isChanged = true;
            return _this.game.redraw();
          }
        });
      });
    }

    return ColorManager;

  })();

}).call(this);
