﻿window.app = window.app ? {}
app = window.app


class app.DrawingContext extends app.Module
	@extend (app.AssetLoader)


	constructor: (@ctx) ->

	width:() -> app.DrawingContext.width
	height:() ->  app.DrawingContext.height
	getAsset: (asset) -> app.DrawingContext.getAsset(asset)

	draw: (obj) ->
		try
			if obj?
				@ctx.putImageData(@getAsset(obj.currentState),obj.x*@width(),obj.y*@height())
		catch err
			console.log err

	clear: (x,y) ->
			@ctx.clearRect x*@width(),y*@height(),@width(),@height()
