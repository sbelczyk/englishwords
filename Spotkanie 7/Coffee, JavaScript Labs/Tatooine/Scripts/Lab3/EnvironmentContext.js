(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.EnvironmentContext = (function() {

    function EnvironmentContext(eventAggregator, drawingCtx, timer) {
      var _this = this;
      this.eventAggregator = eventAggregator;
      this.drawingCtx = drawingCtx;
      this.timer = timer;
      this.keys = 0;
      this.eventAggregator.subscribe('key-collected', (function() {
        return _this.keys++;
      }), this);
      this.eventAggregator.subscribe('key-used', (function() {
        return _this.keys--;
      }), this);
    }

    EnvironmentContext.prototype.delay = function(time, fun) {
      return this.timer.delay(time, fun);
    };

    EnvironmentContext.prototype.initMap = function(width, height) {
      var i, j, _i, _ref1, _results;
      this.width = width;
      this.height = height;
      this.map = [];
      _results = [];
      for (i = _i = 0, _ref1 = height - 1; 0 <= _ref1 ? _i <= _ref1 : _i >= _ref1; i = 0 <= _ref1 ? ++_i : --_i) {
        this.map.push([]);
        _results.push((function() {
          var _j, _ref2, _results1;
          _results1 = [];
          for (j = _j = 0, _ref2 = width - 1; 0 <= _ref2 ? _j <= _ref2 : _j >= _ref2; j = 0 <= _ref2 ? ++_j : --_j) {
            _results1.push(this.map[i].push(null));
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };

    EnvironmentContext.prototype.getKeysNumber = function() {
      return this.keys;
    };

    EnvironmentContext.prototype.getObjAt = function(x, y) {
      if (x > this.width - 1 || y > this.height - 1 || x < 0 || y < 0) {
        return {
          message: "Outside the map",
          outsideMap: true
        };
      }
      return this.map[y][x];
    };

    EnvironmentContext.prototype.setObjAt = function(x, y, obj) {
      this.map[y][x] = obj;
      if (obj != null) {
        obj.x = x;
        obj.y = y;
        return this.drawingCtx.draw(obj);
      } else {
        return this.drawingCtx.clear(x, y);
      }
    };

    EnvironmentContext.prototype.addObj = function(obj) {
      if (obj != null) {
        return this.setObjAt(obj.x, obj.y, obj);
      }
    };

    EnvironmentContext.prototype.moveObjBy = function(x, y, dx, dy) {
      var obj;
      obj = this.getObjAt(x, y);
      this.setObjAt(x, y, null);
      return this.setObjAt(x + dx, y + dy, obj);
    };

    EnvironmentContext.prototype.stepOn = function(x, y, dx, dy) {
      var obj;
      obj = this.getObjAt(x, y);
      if ((obj != null) && (obj.isMoveable != null) && obj.isMoveable) {
        return this.moveObjBy(x, y, dx, dy);
      } else if ((obj != null) && (obj.canGoThrough != null) && obj.canGoThrough) {
        return this.setObjAt(x, y, null);
      }
    };

    EnvironmentContext.prototype.objChangedState = function(obj) {
      return this.drawingCtx.draw(obj);
    };

    EnvironmentContext.prototype.printMap = function(title) {
      var cell, ret, row, _i, _j, _len, _len1, _ref1;
      ret = title;
      _ref1 = this.map;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        row = _ref1[_i];
        ret += "\n";
        for (_j = 0, _len1 = row.length; _j < _len1; _j++) {
          cell = row[_j];
          if (cell != null) {
            switch (this.getObjName(cell)) {
              case 'Wall':
                ret += 'w';
                break;
              case 'LaserBeam':
                ret += '-';
            }
          } else {
            ret += '_';
          }
        }
      }
      return console.log(ret);
    };

    EnvironmentContext.prototype.getObjName = function(obj) {
      var funcNameRegex, results;
      funcNameRegex = /function(.{1,})\(/;
      results = funcNameRegex.exec(obj.constructor.toString());
      if (results && results.length > 1) {
        return results[1].trim();
      } else {
        return "";
      }
    };

    EnvironmentContext.prototype.registerRandomCall = function(obj, fun) {
      return this.timer.registerRandomCall(obj, fun);
    };

    EnvironmentContext.prototype.unregisterRandomCalls = function(obj) {
      return this.timer.unregisterRandomCalls(obj);
    };

    return EnvironmentContext;

  })();

}).call(this);
