(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.BoltWatcher = (function() {

    function BoltWatcher(count, eventAggregator, canvas) {
      var _this = this;
      this.count = count;
      this.eventAggregator = eventAggregator;
      this.canvas = canvas;
      this.eventAggregator.subscribe('bolt-collected', (function() {
        return _this.decrese();
      }), this);
      this.setText();
    }

    BoltWatcher.prototype.decrese = function() {
      var bacground,
        _this = this;
      this.count--;
      this.setText();
      if (this.count === 0) {
        this.eventAggregator.publish('all-bolts-collected');
        bacground = this.canvas.css('background-color');
        this.canvas.css('background-color', 'white');
        return setTimeout((function() {
          return _this.canvas.css('background-color', bacground);
        }), 80);
      }
    };

    BoltWatcher.prototype.setText = function() {
      return $('.bolts-left').text(this.count);
    };

    return BoltWatcher;

  })();

}).call(this);
