﻿window.app = window.app ? {}
app = window.app

class app.BoltWatcher
	constructor: (@count,@eventAggregator,@canvas) ->
		@eventAggregator.subscribe 'bolt-collected', 
									(() => @decrese()),
									this
		@setText()

	decrese: () ->
		@count--		
		@setText()
		if @count == 0
			@eventAggregator.publish 'all-bolts-collected'
			bacground = @canvas.css 'background-color'
			
			@canvas.css 'background-color', 'white'
			setTimeout (()=> @canvas.css 'background-color', bacground),80

	setText: () ->
		$('.bolts-left').text @count