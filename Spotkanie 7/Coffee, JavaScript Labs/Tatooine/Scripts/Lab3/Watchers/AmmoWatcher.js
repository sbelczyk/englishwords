(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.AmmoWatcher = (function() {

    AmmoWatcher.prototype.count = 0;

    function AmmoWatcher(eventAggregator) {
      var _this = this;
      this.eventAggregator = eventAggregator;
      this.eventAggregator.subscribe('ammo-collected', (function(count) {
        return _this.increase(count);
      }), this);
      this.eventAggregator.subscribe('ammo-used', (function() {
        return _this.decrese();
      }), this);
      this.setText();
    }

    AmmoWatcher.prototype.decrese = function() {
      this.count--;
      return this.setText();
    };

    AmmoWatcher.prototype.increase = function(count) {
      this.count += count;
      return this.setText();
    };

    AmmoWatcher.prototype.setText = function() {
      return $('.ammo-left').text(this.count);
    };

    return AmmoWatcher;

  })();

}).call(this);
