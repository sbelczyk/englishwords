﻿window.app = window.app ? {}
app = window.app

class app.AmmoWatcher
	count: 0
	constructor: (@eventAggregator) ->
		@eventAggregator.subscribe 'ammo-collected', 
									((count) => @increase(count)),
									this
		@eventAggregator.subscribe 'ammo-used', 
									(() => @decrese()),
									this
		@setText()

	decrese: () ->
		@count--
		@setText()

	increase: (count) ->
		@count+=count
		@setText()

	setText: () ->
		$('.ammo-left').text @count