﻿window.app = window.app ? {}
app = window.app

class app.KeyWatcher
	count: 0
	constructor: (@eventAggregator) ->
		@eventAggregator.subscribe 'key-collected', 
									(() => @increase()),
									this
		@eventAggregator.subscribe 'key-used', 
									(() => @decrese()),
									this
		@setText()

	decrese: () ->
		@count--
		@setText()

	increase: () ->
		@count++
		@setText()

	setText: () ->
		$('.keys-left').text @count