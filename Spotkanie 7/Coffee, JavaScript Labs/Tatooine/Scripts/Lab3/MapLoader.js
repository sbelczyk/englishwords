(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.MapLoader = (function() {

    function MapLoader(mapStr, envCtx) {
      this.mapStr = mapStr;
      this.envCtx = envCtx;
    }

    MapLoader.prototype.load = function() {
      var char, line, lines, obj, x, y, _i, _j, _len, _len1;
      lines = this.mapStr.split('\n');
      this.envCtx.initMap(lines[0].length - 2, lines.length);
      for (y = _i = 0, _len = lines.length; _i < _len; y = ++_i) {
        line = lines[y];
        line = line.substring(2);
        for (x = _j = 0, _len1 = line.length; _j < _len1; x = ++_j) {
          char = line[x];
          obj = null;
          switch (char) {
            case 'w':
              obj = new app.Wall(this.envCtx, x, y);
              break;
            case 'r':
              obj = new app.Robbo(this.envCtx, x, y);
              break;
            case 'b':
              obj = app.Bolt != null ? new app.Bolt(this.envCtx, x, y) : null;
              break;
            case 'c':
              obj = new app.Container(this.envCtx, x, y);
              break;
            case 's':
              obj = new app.Ship(this.envCtx, x, y);
              break;
            case 'x':
              obj = new app.Bomb(this.envCtx, x, y);
              break;
            case 'k':
              obj = app.Key != null ? new app.Key(this.envCtx, x, y) : null;
              break;
            case '#':
              obj = new app.Rubble(this.envCtx, x, y);
              break;
            case 'd':
              obj = new app.Door(this.envCtx, x, y);
              break;
            case 'a':
              obj = new app.Ammo(this.envCtx, x, y);
              break;
            case '|':
              obj = new app.LaserBeam(this.envCtx, x, y, [0, 1]);
              obj.init();
              break;
            case '-':
              obj = new app.LaserBeam(this.envCtx, x, y, [1, 0]);
              obj.init();
              break;
            case '>':
              obj = new app.Laser(this.envCtx, x, y, 'right');
              break;
            case 'v':
              obj = new app.Laser(this.envCtx, x, y, 'down');
              break;
            case '<':
              obj = new app.Laser(this.envCtx, x, y, 'left');
              break;
            case '^':
              obj = new app.Laser(this.envCtx, x, y, 'up');
          }
          if (obj !== null) {
            this.envCtx.setObjAt(x, y, obj);
          }
        }
      }
    };

    return MapLoader;

  })();

}).call(this);
