﻿window.app = window.app ? {}
app = window.app




class app.Game 
	constructor: (@canvas,mapStr) ->
		@canvasContext2D = @canvas.get(0).getContext('2d')
		@eventAggregator = new app.EventAggregator()
		#@boltWatcher = new app.BoltWatcher(mapStr.split('').where((c)=>c is 'b').length,@eventAggregator,@canvas)
		#@keyWatcher = new app.KeyWatcher @eventAggregator
		@keyboardWatcher = new app.KeyboardWatcher @eventAggregator
		#@ammoWatcher = new app.AmmoWatcher @eventAggregator		
		@drawingCtx = new app.DrawingContext @canvasContext2D
		@timeDelayedMethodCall = new app.TimeDelayedMethodCall()		
		@map=[]
		@envCtx = new app.EnvironmentContext @eventAggregator,@drawingCtx,@timeDelayedMethodCall
		@levelManager = new app.LevelManger @envCtx
		mapLoader = new app.MapLoader mapStr,@envCtx
		mapLoader.load()


	redraw: () ->
			for x in [0..@envCtx.width-1]
				for y in [0..@envCtx.height-1]
					obj = @envCtx.getObjAt(x,y)
					if obj?
						@envCtx.objChangedState(obj)

			app.ColorTranslation.isChanged = false;
			return
