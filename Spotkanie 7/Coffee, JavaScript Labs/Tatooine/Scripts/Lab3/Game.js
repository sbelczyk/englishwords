(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.Game = (function() {

    function Game(canvas, mapStr) {
      var mapLoader;
      this.canvas = canvas;
      this.canvasContext2D = this.canvas.get(0).getContext('2d');
      this.eventAggregator = new app.EventAggregator();
      this.keyboardWatcher = new app.KeyboardWatcher(this.eventAggregator);
      this.drawingCtx = new app.DrawingContext(this.canvasContext2D);
      this.timeDelayedMethodCall = new app.TimeDelayedMethodCall();
      this.map = [];
      this.envCtx = new app.EnvironmentContext(this.eventAggregator, this.drawingCtx, this.timeDelayedMethodCall);
      this.levelManager = new app.LevelManger(this.envCtx);
      mapLoader = new app.MapLoader(mapStr, this.envCtx);
      mapLoader.load();
    }

    Game.prototype.redraw = function() {
      var obj, x, y, _i, _j, _ref1, _ref2;
      for (x = _i = 0, _ref1 = this.envCtx.width - 1; 0 <= _ref1 ? _i <= _ref1 : _i >= _ref1; x = 0 <= _ref1 ? ++_i : --_i) {
        for (y = _j = 0, _ref2 = this.envCtx.height - 1; 0 <= _ref2 ? _j <= _ref2 : _j >= _ref2; y = 0 <= _ref2 ? ++_j : --_j) {
          obj = this.envCtx.getObjAt(x, y);
          if (obj != null) {
            this.envCtx.objChangedState(obj);
          }
        }
      }
      app.ColorTranslation.isChanged = false;
    };

    return Game;

  })();

}).call(this);
