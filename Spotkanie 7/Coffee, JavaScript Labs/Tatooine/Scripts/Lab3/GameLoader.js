(function() {
  var app, mapStr, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  mapStr = "1 wwwwwwwwwwwwwwww\n2 wr_b_c_c_______w\n3 w_b_c_cc_______w\n4 wccc__ccc______w\n5 w______cc______w\n6 w_____c________w\n7 w______________w\n8 w_____k________w\n9 w______________w\n10w______________w\n11w______________w\n12w______________w\n13w______________w\n14w______________w\n15w______________w\n16wwwwwwwwwwwwwwww";

  app.ColorTranslation = [
    {
      from: [162, 114, 64, 255],
      to: [102, 114, 64, 255]
    }, {
      from: [28, 39, 131, 255],
      to: [28, 39, 129, 255]
    }, {
      from: [16, 16, 16, 255],
      to: [16, 16, 16, 255]
    }, {
      from: [152, 152, 152, 255],
      to: [152, 152, 152, 255]
    }, {
      from: [0, 0, 0, 0],
      to: [0, 0, 0, 0]
    }
  ];

  $(window).load(function() {
    var game;
    game = new app.Game($('.game-board'), mapStr);
    window.game = game;
    return new app.ColorManager($('.game-board'), game);
  });

}).call(this);
