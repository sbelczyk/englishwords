﻿window.app = window.app ? {}
app = window.app

	
class app.Robbo extends app.Object

	@include (new app.Multistate)

	constructor: (@envCtx,@x,@y) ->
				super('robbo-fu')

				@currentState = 'robbo-fu'

				@eventAggregator.subscribe 'arrow-down',
											((args)=>@arrowDown(args)),
											this

					


	stateSelector: (delta,currentState, args...) ->
					s = currentState[6]
					ms = currentState[7]
					ms = if ms=='u' then 'd' else 'u'
					if ((s=='f' or s=='l')and delta[0] == -1 and delta[1]== 0)
						return 'robbo-l'+ms
					else if ((s=='f' or s=='r')and delta[0] == 1 and delta[1]== 0)
						return 'robbo-r'+ms
					else if (s=='f' and delta[0] == 0 and delta[1]== 1)
						return 'robbo-f'+ms
					else if ((s=='f' or s=='u') and delta[0] == 0 and delta[1]== -1)
						return 'robbo-u'+ms
					return 'robbo-fu'


	arrowDown: (evnetArgs) ->
		if (not evnetArgs.ctrl)
			@makeMove(evnetArgs.direction)
		else if @ammo>0
			@fire(evnetArgs.direction)



	makeMove: (direction) ->
		delta = @getDelta(direction)
		newX = @x+delta[0]
		newY = @y+delta[1]
		obj = @envCtx.getObjAt(newX,newY)
		if obj is null or (obj.canStepOn? and obj.canStepOn(delta))
			@envCtx.stepOn(newX,newY, delta[0],delta[1])
			@envCtx.moveObjBy @x,@y, delta[0],delta[1]
			@eventAggregator.publish 'robbo-moved',@x,@y,delta
			@envCtx.delay 50,()=> @changeState delta

	getDelta: (direction) ->
		switch direction
			when 'left' then delta = [-1,0]
			when 'right' then delta = [1,0]
			when 'up' then delta = [0,-1]
			when 'down' then delta = [0,1]


