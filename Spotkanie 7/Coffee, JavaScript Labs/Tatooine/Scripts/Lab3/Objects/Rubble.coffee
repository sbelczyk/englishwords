﻿window.app = window.app ? {}
app = window.app

class app.Rubble extends app.Object
	
	constructor: (@envCtx,@x,@y) ->
				super('rubble')
	canStepOn: () ->
		false

	canBlowUp: () ->
		true

	blowUp: () ->
		smoke = new app.Smoke(@envCtx,@x,@y)
		@envCtx.setObjAt(@x,@y,smoke)
		smoke.init()