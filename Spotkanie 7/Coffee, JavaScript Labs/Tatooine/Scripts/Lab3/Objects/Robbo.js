(function() {
  var app, _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __slice = [].slice;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.Robbo = (function(_super) {

    __extends(Robbo, _super);

    Robbo.include(new app.Multistate);

    function Robbo(envCtx, x, y) {
      var _this = this;
      this.envCtx = envCtx;
      this.x = x;
      this.y = y;
      Robbo.__super__.constructor.call(this, 'robbo-fu');
      this.currentState = 'robbo-fu';
      this.eventAggregator.subscribe('arrow-down', (function(args) {
        return _this.arrowDown(args);
      }), this);
    }

    Robbo.prototype.stateSelector = function() {
      var args, currentState, delta, ms, s;
      delta = arguments[0], currentState = arguments[1], args = 3 <= arguments.length ? __slice.call(arguments, 2) : [];
      s = currentState[6];
      ms = currentState[7];
      ms = ms === 'u' ? 'd' : 'u';
      if ((s === 'f' || s === 'l') && delta[0] === -1 && delta[1] === 0) {
        return 'robbo-l' + ms;
      } else if ((s === 'f' || s === 'r') && delta[0] === 1 && delta[1] === 0) {
        return 'robbo-r' + ms;
      } else if (s === 'f' && delta[0] === 0 && delta[1] === 1) {
        return 'robbo-f' + ms;
      } else if ((s === 'f' || s === 'u') && delta[0] === 0 && delta[1] === -1) {
        return 'robbo-u' + ms;
      }
      return 'robbo-fu';
    };

    Robbo.prototype.arrowDown = function(evnetArgs) {
      if (!evnetArgs.ctrl) {
        return this.makeMove(evnetArgs.direction);
      } else if (this.ammo > 0) {
        return this.fire(evnetArgs.direction);
      }
    };

    Robbo.prototype.makeMove = function(direction) {
      var delta, newX, newY, obj,
        _this = this;
      delta = this.getDelta(direction);
      newX = this.x + delta[0];
      newY = this.y + delta[1];
      obj = this.envCtx.getObjAt(newX, newY);
      if (obj === null || ((obj.canStepOn != null) && obj.canStepOn(delta))) {
        this.envCtx.stepOn(newX, newY, delta[0], delta[1]);
        this.envCtx.moveObjBy(this.x, this.y, delta[0], delta[1]);
        this.eventAggregator.publish('robbo-moved', this.x, this.y, delta);
        return this.envCtx.delay(50, function() {
          return _this.changeState(delta);
        });
      }
    };

    Robbo.prototype.getDelta = function(direction) {
      var delta;
      switch (direction) {
        case 'left':
          return delta = [-1, 0];
        case 'right':
          return delta = [1, 0];
        case 'up':
          return delta = [0, -1];
        case 'down':
          return delta = [0, 1];
      }
    };

    return Robbo;

  })(app.Object);

}).call(this);
