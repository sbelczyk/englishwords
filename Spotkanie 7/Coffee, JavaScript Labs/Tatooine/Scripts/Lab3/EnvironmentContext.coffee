﻿window.app = window.app ? {}
app = window.app


class app.EnvironmentContext
	constructor: (@eventAggregator,@drawingCtx,@timer)->
		@keys = 0
		@eventAggregator.subscribe 'key-collected', 
									(() => @keys++),
									this
		@eventAggregator.subscribe 'key-used', 
									(() => @keys--),
									this
	delay: (time, fun) ->
		@timer.delay time, fun

	initMap : (width,height) ->
		@width = width
		@height = height
		@map = []
		for i in [0..height-1]
			@map.push []
			for j in [0..width-1]
				@map[i].push null

	getKeysNumber: ->
			return @keys

	getObjAt: (x,y) ->
		if (x>@width-1 or y>@height-1 or x<0 or y<0)
			return {message: "Outside the map",outsideMap: true}
		@map[y][x]

	setObjAt: (x,y,obj) ->
		@map[y][x]=obj
		if obj?
			obj.x = x
			obj.y = y
			@drawingCtx.draw obj
		else
			@drawingCtx.clear x,y

	addObj: (obj) ->
		@setObjAt(obj.x,obj.y,obj) if obj?

	moveObjBy: (x,y,dx,dy) ->
		obj = @getObjAt(x,y)
		@setObjAt x,y,null
		@setObjAt x+dx,y+dy,obj
		

	stepOn: (x,y,dx,dy) ->
		obj = @getObjAt(x,y)
		if obj? and obj.isMoveable? and obj.isMoveable
			@moveObjBy(x,y,dx,dy)
		else if obj? and obj.canGoThrough? and obj.canGoThrough
			@setObjAt x,y,null


	objChangedState: (obj) ->
		@drawingCtx.draw obj

	printMap: (title) ->
		ret = title
		for row in @map
			ret+="\n"
			for cell in row
				if cell?
					switch @getObjName(cell)
						when 'Wall'
							ret+='w'
						when 'LaserBeam'
							ret+='-'
				else
					ret+='_'
		console.log ret
					
	getObjName:	(obj)->
			funcNameRegex = ///function (.{1,})\(///
			results = (funcNameRegex).exec((obj).constructor.toString());
			return if (results && results.length > 1) then results[1].trim() else "";

	registerRandomCall: (obj,fun) ->
		@timer.registerRandomCall obj,fun
	unregisterRandomCalls: (obj) ->
		@timer.unregisterRandomCalls obj