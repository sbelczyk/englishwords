(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.LevelManger = (function() {

    function LevelManger(envCtx) {
      var _this = this;
      this.envCtx = envCtx;
      this.envCtx.eventAggregator.subscribe('robbo-destroyed', (function() {
        return _this.onRobboDestroyed();
      }));
    }

    LevelManger.prototype.onRobboDestroyed = function() {
      var _this = this;
      return this.envCtx.delay(700, function() {
        var obj, smoke, x, y, _i, _ref1, _results;
        _results = [];
        for (y = _i = 0, _ref1 = _this.envCtx.height - 1; 0 <= _ref1 ? _i <= _ref1 : _i >= _ref1; y = 0 <= _ref1 ? ++_i : --_i) {
          _results.push((function() {
            var _j, _ref2, _results1;
            _results1 = [];
            for (x = _j = 0, _ref2 = this.envCtx.width - 1; 0 <= _ref2 ? _j <= _ref2 : _j >= _ref2; x = 0 <= _ref2 ? ++_j : --_j) {
              obj = this.envCtx.getObjAt(x, y);
              if ((obj != null) && this.envCtx.getObjName(obj) !== 'Smoke') {
                smoke = new app.Smoke(this.envCtx, obj.x, obj.y);
                this.envCtx.setObjAt(x, y, smoke);
                this.envCtx.eventAggregator.unsubscribe(obj);
                this.envCtx.unregisterRandomCalls(obj);
                _results1.push(smoke.init());
              } else {
                _results1.push(void 0);
              }
            }
            return _results1;
          }).call(_this));
        }
        return _results;
      });
    };

    return LevelManger;

  })();

}).call(this);
