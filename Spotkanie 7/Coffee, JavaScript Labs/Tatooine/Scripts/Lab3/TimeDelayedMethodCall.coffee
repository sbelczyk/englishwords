﻿window.app = window.app ? {}
app = window.app

class app.TimeDelayedMethodCall
	constructor: () ->
		@randomCallers = []

	delay: (time,fun) ->
		setTimeout(fun,time)

	registerRandomCall: (obj,fun) ->
		@randomCallers.push {obj: obj, fun: fun}
		@randomTrigger obj,fun

	unregisterRandomCalls: (obj) ->
		for rCall,i in @randomCallers
			if rCall? and (rCall.obj==obj)
					 @randomCallers[i] = null
			 @randomCallers =  @randomCallers.where (i)->i?
		return


	randomTrigger: (obj,fun) ->
		rc = @randomCallers.single (c)-> c.obj==obj and c.fun==fun
		if (rc?)
			time = @getRand(1,15)*300
			callback = ()=>		
								rc = @randomCallers.single (c)-> c.obj==obj and c.fun==fun
								if (rc?)
									rc.fun()
									@randomTrigger(rc.obj,rc.fun)
			setTimeout(callback,time)

	getRand: (x,y) ->
		Math.floor((Math.random()*y)+x)
		

	