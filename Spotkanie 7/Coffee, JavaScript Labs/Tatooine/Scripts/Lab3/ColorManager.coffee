﻿window.app = window.app ? {}
app = window.app

class app.ColorManager
	constructor: (canvas,@game) ->
			bs = $('.background-color-selector')
			start ='#'+bs.attr('data-start-val')
			bs.css('background-color',start)
			bs.ColorPicker {
							color: bs.attr('data-start-val'),
							onSubmit: (hsb, hex, rgb,el) ->
										bs.css('background-color','#'+hex)
										canvas.css('background-color','#'+hex)

							}
			$('.color-selector').each (i,e)=>
										startVal = $(e).attr('data-start-val')
										$(e).css('background-color',"##{startVal}")
										$(e).ColorPicker {
														color: "##{startVal}",
														onSubmit: (hsb, hex, rgb,el) =>
																	$(el).css('background-color','#'+hex)
																	to = app.ColorTranslation[$(el).attr('data-for')].to
																	to[0]=rgb.r
																	to[1]=rgb.g
																	to[2]=rgb.b
																	app.ColorTranslation.isChanged = true;
																	@game.redraw()
														}
		 