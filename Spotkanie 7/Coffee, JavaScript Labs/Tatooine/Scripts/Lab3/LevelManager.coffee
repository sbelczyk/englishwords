﻿window.app = window.app ? {}
app = window.app

class app.LevelManger 
	constructor: (@envCtx) ->
			@envCtx.eventAggregator.subscribe 'robbo-destroyed',(()=>@onRobboDestroyed())

	onRobboDestroyed: ()->
		@envCtx.delay(700,() =>
							for y in [0..@envCtx.height-1]
								for x in [0..@envCtx.width-1]
									obj = @envCtx.getObjAt x,y
									if obj? and @envCtx.getObjName(obj) isnt 'Smoke'
										smoke = new app.Smoke @envCtx,obj.x,obj.y
										@envCtx.setObjAt x,y, smoke
										@envCtx.eventAggregator.unsubscribe obj
										@envCtx.unregisterRandomCalls obj
										smoke.init()
						)
