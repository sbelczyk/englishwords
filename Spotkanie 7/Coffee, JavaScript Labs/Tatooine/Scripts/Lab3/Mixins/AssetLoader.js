(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.AssetLoader = (function() {

    function AssetLoader() {}

    AssetLoader.assets = {};

    AssetLoader.width = 32;

    AssetLoader.height = 32;

    AssetLoader.getAsset = function(name) {
      var asset;
      if (app.ColorTranslation.isChanged) {
        this.assets = {};
        app.ColorTranslation.isChanged = false;
      }
      asset = this.assets[name];
      if (!(asset != null)) {
        this.assets[name] = this.loadAsset(name);
      }
      return this.assets[name];
    };

    AssetLoader.loadAsset = function(name) {
      var canvas, ctx, i, imageData, img, trans, _i, _j, _len, _ref1;
      img = $("." + name).clone().get(0);
      canvas = $('.assetLoad').get(0);
      ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      ctx.drawImage(img, 0, 0);
      imageData = ctx.getImageData(0, 0, this.width, this.height);
      for (i = _i = 0; _i <= 1023; i = ++_i) {
        _ref1 = app.ColorTranslation;
        for (_j = 0, _len = _ref1.length; _j < _len; _j++) {
          trans = _ref1[_j];
          if (this.compareColor(trans.from, this.getPixel(imageData, i))) {
            this.setPixel(imageData, i, trans.to);
          }
        }
      }
      return imageData;
    };

    AssetLoader.getPixel = function(data, n) {
      return [data.data[n * 4], data.data[n * 4 + 1], data.data[n * 4 + 2], data.data[n * 4 + 3]];
    };

    AssetLoader.compareColor = function(col1, col2) {
      return col1[0] === col2[0] && col1[1] === col2[1] && col1[2] === col2[2] && col1[3] === col2[3];
    };

    AssetLoader.setPixel = function(data, n, color) {
      var i, _i, _results;
      _results = [];
      for (i = _i = 0; _i <= 3; i = ++_i) {
        _results.push(data.data[n * 4 + i] = color[i]);
      }
      return _results;
    };

    return AssetLoader;

  })();

}).call(this);
