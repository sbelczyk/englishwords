﻿window.app = window.app ? {}
app = window.app

class app.Blowable
	
	constructor: () ->
	
	canBlowUp: () ->
		true

	blowUp: () ->
		smoke = new app.Smoke(@envCtx,@x,@y)
		@envCtx.setObjAt(@x,@y,smoke)
		smoke.init()
		@envCtx.eventAggregator.unsubscribe this
		if @onBlowUp?
			@onBlowUp()