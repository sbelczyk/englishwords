(function() {
  var app, _ref;

  window.app = (_ref = window.app) != null ? _ref : {};

  app = window.app;

  app.Blowable = (function() {

    function Blowable() {}

    Blowable.prototype.canBlowUp = function() {
      return true;
    };

    Blowable.prototype.blowUp = function() {
      var smoke;
      smoke = new app.Smoke(this.envCtx, this.x, this.y);
      this.envCtx.setObjAt(this.x, this.y, smoke);
      smoke.init();
      this.envCtx.eventAggregator.unsubscribe(this);
      if (this.onBlowUp != null) {
        return this.onBlowUp();
      }
    };

    return Blowable;

  })();

}).call(this);
