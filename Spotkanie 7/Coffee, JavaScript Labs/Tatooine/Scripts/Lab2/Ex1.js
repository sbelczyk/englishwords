﻿$(function () {
    var FormList;
    FormList = function () {
        function FormList(element, maxElements) {
            this.element = element;
            this.maxElements = maxElements;
            this.template = this.element.find(".template");
            this.effect = "drop";
            this.createButtons(this.template);
            this.template.find(".removeBtn").hide();
        };

        FormList.prototype.createButtons = function (item) {
            var addBtn, removeBtn;
            addBtn = $('<button class="addBtn" type="button">Add<\/button>');
            removeBtn = $('<button type="button" class="removeBtn">Remove<\/button>');
            item.append(addBtn);
            item.append(removeBtn);
            return this.bindButtons(item);
        };

        FormList.prototype.bindButtons = function (item) {
            var _this = this;
            item.find(".removeBtn").click(function () {
                return _this.removeItem(item);
            });
            item.find(".addBtn").click(function () {
                return _this.addItem(item);
            });
        };

        FormList.prototype.removeItem = function (item) {
            var _this = this;
            item.hide(this.effect, function () {
                item.remove();
                _this.recalculateIndexes();
                _this.updateButtonsVisibility();
            });
        };

        FormList.prototype.addItem = function (item) {
            var t;
            if (this.numberOfItems() < this.maxElements) {
                t = this.template.clone();
                t.removeClass("template");
                t.hide();
                item.after(t);
                t.show(this.effect);
                this.bindButtons(t);
                this.recalculateIndexes();
                this.updateButtonsVisibility();
            }
        };

        FormList.prototype.updateButtonsVisibility = function () {
            if (this.numberOfItems() === this.maxElements) {
                this.element.find(".addBtn").hide();
                this.element.find(".removeBtn").show();
            }
            else {
                if (this.numberOfItems() === 1) {
                    this.element.find(".removeBtn").hide(), this.element.find(".addBtn").show();
                } else {
                    this.element.find(".removeBtn,.addBtn").show();
                }
            }
            };

        FormList.prototype.recalculateIndexes = function () {
            var n, r, t, u, items;
            for (items = this.element.find(".item"), n = t = 0, u = items.length; t < u; n = ++t) {
                r = items[n];
                $(r).find(".index").text(n + 1);
            }
        };

        FormList.prototype.numberOfItems = function () {
            return this.element.find(".item").size();
        };

        return FormList;
    }();

    $(function () {
        return new FormList($(".users2"), 3);
    });
});