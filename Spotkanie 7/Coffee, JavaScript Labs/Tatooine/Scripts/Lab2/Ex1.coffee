﻿window.app = window.app ? {}
app = window.app

class app.FormList

	constructor: (@element,@maxElements) ->
		@template = @element.find('.template')		
		@effect = 'drop'
		@.createButtons(@template)
		@template.find('.removeBtn').hide()

	createButtons: (item) ->
		addBtn = $ '<button class="addBtn" type="button">Add</button>'
		removeBtn = $ '<button type="button" class="removeBtn">Remove</button>'
		item.append(addBtn)
		item.append(removeBtn)
		@.bindButtons(item)


	bindButtons: (item) ->
		item.find('.removeBtn').click () => @removeItem(item)
		item.find('.addBtn').click () => @addItem(item)

	removeItem: (item) ->		
			item.hide(@effect, => 
								item.remove()
								@recalculateIndexes()	
								@updateButtonsVisibility()
					)


	addItem: (afterItem) ->
			return unless @.numberOfItems()<@maxElements
			newItem = @template.clone()
			newItem.removeClass('template')
			newItem.hide()
			afterItem.after(newItem)						
			newItem.show @effect
			@bindButtons(newItem)
			@recalculateIndexes()
			@updateButtonsVisibility()

	updateButtonsVisibility: () ->
		if (@numberOfItems()==@maxElements)
			@element.find('.addBtn').hide()
			@element.find('.removeBtn').show()
		else if (@numberOfItems()==1)
			@element.find('.removeBtn').hide()
			@element.find('.addBtn').show()
		else
			@element.find('.removeBtn,.addBtn').show()
			

	recalculateIndexes: ->
		for item,i in @element.find('.item')
			$(item).find('.index').text i+1
		return

	numberOfItems: ->
		@element.find('.item').size()
