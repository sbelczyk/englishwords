﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;

namespace EnglishWords.Controllers
{
    public class WordController : Controller
    {
	    private IWordsRepository wordsRepository;
	    public WordController()
	    {
			wordsRepository = new WordsRepository();
	    }
        //
        // GET: /Word/

        public ActionResult Index()
        {
            return View(wordsRepository.FindAll());
        }

		public ActionResult AddWord()
		{
			return View(new Word());
		}

		[HttpPost]
		public ActionResult AddWord(Word word)
		{
			wordsRepository.Add(word);

			ViewBag.Message = "Word has been added.";
			return View();
		}
    }
}
